import networkx as nx
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from scipy.optimize import minimize_scalar
from scipy.optimize import minimize
import math

class marg_stat:

    __method_name = ''

    def __init__(self, data, method_name):
        self.__data = data
        self.__method_name = method_name

    def __dict_fr(self, in_out):
        if self.__method_name == "md":
            name = ''
        else:
            name = '_' + self.__method_name
        d = {x: data[in_out+'_degree'+name].tolist().count(x) for x in data[in_out+'_degree'+name].tolist()}
        return d

    def pr(self,param):
        print(self.__dict_fr(param))

    def compute(self, ras):
        return {
            "md": self.__md(ras),
            "me": self.__me(ras),
        }[self.__method_name]

    def save(self, ras='p'):
        self.__create_data()
        if self.__method_name == "md":
            name = ''
        else:
            name = '_' + self.__method_name
        df = pd.DataFrame(self.__data,
                           columns=['bank', 'in_degree'+name, ras+'_in'+name,
                                    'lg_in'+name,'lg_'+ras+'_in'+name, 'out_degree'+name,
                                    ras+'_out'+name, 'lg_out'+name, 'lg_'+ras+'_out'+name])
        writer = pd.ExcelWriter('Ru_logs_'+name+'-'+ras+'.xlsx', engine='xlsxwriter')
        df.to_excel(writer, sheet_name='Sheet1')
        writer.save()

    def __error(self, w1, x, y, w0):
        return ((y - (w0 + w1 * x))**2).mean()

    def __linear_func(self, w1, x, w0=50):
        return w0 + w1 * x

    def __chast(self, val, size, d):
        for k, v in d.items():
            if val == k:
                return v / size

    def __my_lg(self, val):
        if val == 0:
            val = float('nan')
        return math.log(val)

    def __create_data(self):
        if self.__method_name == "md":
            name = ''
        else:
            name = '_' + self.__method_name
        self.__data['p_in'+name] = self.__data.apply(
            lambda row: row['in_degree'+name] / sum(self.__data['in_degree'+name]), axis=1)
        self.__data['lg_in'+name] = self.__data.apply(
            lambda row: self.__my_lg(row['in_degree'+name]), axis=1)
        self.__data['lg_p_in'+name] = self.__data.apply(
            lambda row: self.__my_lg(row['p_in'+name]), axis=1)
        self.__data['p_out'+name] = self.__data.apply(
            lambda row: row['out_degree'+name] / sum(self.__data['out_degree'+name]), axis=1)
        self.__data['lg_out'+name] = self.__data.apply(
            lambda row: self.__my_lg(row['out_degree'+name]), axis=1)
        self.__data['lg_p_out'+name] = self.__data.apply(
            lambda row: self.__my_lg(row['p_out'+name]), axis=1)
        self.__data['test_in'+name] = self.__data.apply(
            lambda row: self.__linear_func(-3.83, row['lg_in'+name], w0=5.52), axis=1)
        self.__data['test_out'+name] = self.__data.apply(
            lambda row: self.__linear_func(-3.83, row['lg_in'+name], w0=5.52), axis=1)
        self.__data['fr_in'+name] = self.__data.apply(
            lambda row: self.__chast(row['in_degree'+name], len(data['in_degree'+name]), self.__dict_fr('in')), axis=1)
        self.__data['fr_out'+name] = data.apply(
            lambda row: self.__chast(row['out_degree'+name], len(data['out_degree'+name]), self.__dict_fr('out')), axis=1)
        self.__data['lg_fr_in'+name] = self.__data.apply(
            lambda row: self.__my_lg(row['fr_in'+name]), axis=1)
        self.__data['lg_fr_out'+name] = data.apply(
            lambda row: self.__my_lg(row['fr_out'+name]), axis=1)


    def plot_f(self, in_out, fr_p, ras='degree'):
        self.__create_data()
        if self.__method_name == "md":
            name = ''
        else:
            name = '_' + self.__method_name
        df = pd.DataFrame(data.drop(1))
        param_x = in_out + '_' + ras + name
        param_y = fr_p + '_' + in_out + name
        if ras == 'lg':
            param_x = ras + '_' + in_out + name
            param_y = ras + '_' + param_y
        ax = df.plot.scatter(x=param_x, y=param_y)
        ax.set_axis_bgcolor('w')


    def __min_func(self, ras):
        if self.__method_name == "md":
            name = ''
        else:
            name = '_' + self.__method_name
        my_func_in = lambda w1_w0: self.__error(w1_w0[0], data['lg_in'+name], data['lg_'+ras+'_in'+name], w1_w0[1])
        my_func_out = lambda w1_w0: self.__error(w1_w0[0], data['lg_out' + name], data['lg_'+ras+'_out' + name], w1_w0[1])
        res_in = minimize(my_func_in, bounds=((-5, 5), (-100, 100)), x0=(0, 0), method='L-BFGS-B')
        res_out = minimize(my_func_out, bounds=((-5, 5), (-100, 100)), x0=(0, 0), method='L-BFGS-B')
        coef = {"in":{
                    "a": res_in.x[1],
                    "b": res_in.x[0]},
                "out": {
                    "a": res_out.x[1],
                    "b": res_out.x[0],
                }}
        return coef

    def __md(self, ras):
        self.__create_data()
        return self.__min_func(ras)


    def __me(self, ras):
        self.__create_data()
        return self.__min_func(ras)



data = pd.read_csv('Ru_banks_v100.csv', index_col='Index')
a = marg_stat(data, 'me').plot_f('out', 'fr')

plt.show()



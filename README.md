# My project's README
To install project use pip req, you need the anaconda 3 package to work with project

# Initial class

marg_stat(data, method_name) 
data - input data must be pandas object
method_name - name of method which you need to use to explore a network,may be 'me' or 'md'
'me' - maximum entropy method
'md' - minimum density method

a = marg_sata(
	pandas.read_csv('Ru_banks_v100.csv'),
	'me'
	)

# Methods
1)plot_f(in_out,fr_p,ras='degree') - ploting a graph where in_out - in or out degree of graph, 
fr_p - friquence or probability
ras- may be lf if u want mapping to log-log space


2)save(ras='p') - save your topology's in to exel file. 
res='p' - means what you will create a probability data 
res='fr' - for friquence data

3)compute(ras)  - method to compute the linear regression of function. 
It use L-BFGS-B method. 
Parametr ras may be 'fr' or 'p' 

# example:
a.plot_f('out', 'fr') - will plot f(x)=x where f(x)- friquence of out degree links x- out degree edges 

a.save(ras='fr') - will create a Ru_logs_me-fr.xlsx file which contains data of network

a.compute('fr') - will compute linear regression paramaters for friquence function